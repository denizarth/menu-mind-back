package br.com.easymind.repository.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
@Table(schema = "easycardapio", name = "CARDAPIO_ITEM")
public class CardapioItem {

	@Id
	@Column(name = "ID_CARDAPIO_ITEM")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idCardapioItem;

	@Column(name = "ID_CARDAPIO")
	private Integer idCardapio;
	
	@Column(name = "ID_CARDAPIO_SECAO")
	private Integer idCardapioSecao;

	@Column(name = "NOME")
	private String nome;

	@Column(name = "CODIGO_RESTAURANTE")
	private String codigoRestaurante;

	@Column(name = "IMAGEM")
	private String imagem;
	
	@Column(name = "ITEM_ADICIONAL")
	private String itemAdicional; 
	
	@Column(name = "PRECO")
	private Double preco;
		
}