package br.com.easymind.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import br.com.easymind.repository.data.ContaPedidoItem;

public interface ContaPedidoItemRepository extends JpaRepository<ContaPedidoItem, Integer> {

	List<ContaPedidoItem> findAllByIdConta(Integer id);

	List<ContaPedidoItem> findAllByIsReady(boolean isReady);
	

}
