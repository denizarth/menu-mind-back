package br.com.easymind.repository.data;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
@Table(schema = "easycardapio", name = "CONTA_PEDIDO")
public class ContaPedido {

	@Id
	@Column(name = "ID_CONTA_PEDIDO")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idContaPedido;

	@Column(name = "ID_CONTA")
	private Integer idConta;
	/*
	@ManyToMany
	@JoinTable(name = "CONTA_PEDIDO_ITEM", joinColumns = {
			@JoinColumn(name = "ID_CONTA_PEDIDO") }, inverseJoinColumns = {
					@JoinColumn(name = "ID_CARDAPIO_ITEM") })
	private List<CardapioItem> cardapioItems;
	*/
}