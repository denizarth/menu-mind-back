package br.com.easymind.http;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.easymind.repository.ContaPedidoItemRepository;
import br.com.easymind.repository.data.ContaPedidoItem;
import lombok.RequiredArgsConstructor;

@CrossOrigin(origins="*", allowedHeaders="*")
@RequestMapping(path = "conta")
@RestController
@RequiredArgsConstructor
public class ContaController {

//	private final ContaPedidoRepository contaPedidoRepository;
	
	private final ContaPedidoItemRepository contaPedidoItemRepository ;

	
	@GetMapping("itens")
	public @ResponseBody List<ContaPedidoItem> getItensPedido(@RequestParam Integer idConta) {
		return contaPedidoItemRepository.findAllByIdConta(idConta);
	}
	
	@GetMapping("allitens")
	public @ResponseBody List<ContaPedidoItem> getAllItensPedido() {
		return contaPedidoItemRepository.findAllByIsReady(false);
	}
	
	@PutMapping("item/{idContaPedidoItem}")
	public @ResponseBody ContaPedidoItem setIsReady(@PathVariable Integer idContaPedidoItem) {		
		System.out.println("recebeu: " + idContaPedidoItem); 
		
		Optional<ContaPedidoItem> item = contaPedidoItemRepository.findById(idContaPedidoItem);
		if ( !Objects.isNull(( item.get() ))) {
			item.get().setIsReady(true);
			contaPedidoItemRepository.save(item.get()); 
			return item.get();
		}
		return null;	

	}
	
	@GetMapping("item/{idContaPedidoItem}")
	public @ResponseBody Optional<ContaPedidoItem> getContaPedidoItem(@PathVariable Integer idContaPedidoItem) {		
		return contaPedidoItemRepository.findById(idContaPedidoItem);

	}
	
	@PostMapping("item")
	public @ResponseBody ContaPedidoItem postContaPedidoItem(@RequestBody ContaPedidoItem contaPedidoItems) {		
		return contaPedidoItemRepository.save(contaPedidoItems);

	}

	@PostMapping("itens")
	public @ResponseBody List<ContaPedidoItem> postContaPedidoItens(@RequestBody List<ContaPedidoItem> contaPedidoItems) {		
		System.out.println(contaPedidoItems);
		return contaPedidoItemRepository.saveAll(contaPedidoItems);

	}
}
