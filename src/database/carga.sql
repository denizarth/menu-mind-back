DELETE FROM easycardapio.cardapio;
DELETE FROM easycardapio.cardapio_secao;

INSERT INTO easycardapio.cardapio (ID_CARDAPIO, NOME) VALUES(1, 'cardapio charrete');
INSERT INTO easycardapio.cardapio (ID_CARDAPIO, NOME) VALUES(2, 'cardapio pizzaria Bertis');

INSERT INTO easycardapio.cardapio_secao (id_cardapio_secao, id_CARDAPIO, NOME, IMAGEM, ORDEM) VALUES(1, 1, 'Prato do dia', 'fast-food', 1);
INSERT INTO easycardapio.cardapio_secao (id_cardapio_secao, id_CARDAPIO, NOME, IMAGEM, ORDEM) VALUES(2, 1, 'Carnes', 'home', 2);
INSERT INTO easycardapio.cardapio_secao (id_cardapio_secao, id_CARDAPIO, NOME, IMAGEM, ORDEM) VALUES(3, 1, 'Saladas', 'leaf', 3);
INSERT INTO easycardapio.cardapio_secao (id_cardapio_secao, id_CARDAPIO, NOME, IMAGEM, ORDEM) VALUES(4, 1, 'Carnes', 'nutrition', 4);
INSERT INTO easycardapio.cardapio_secao (id_cardapio_secao, id_CARDAPIO, NOME, IMAGEM, ORDEM) VALUES(5, 1, 'Aves', 'heart', 5);
INSERT INTO easycardapio.cardapio_secao (id_cardapio_secao, id_CARDAPIO, NOME, IMAGEM, ORDEM) VALUES(6, 1, 'Peixes', 'fish', 6);
INSERT INTO easycardapio.cardapio_secao (id_cardapio_secao, id_CARDAPIO, NOME, IMAGEM, ORDEM) VALUES(7, 1, 'Bebidas', 'pint', 7);
INSERT INTO easycardapio.cardapio_secao (id_cardapio_secao, id_CARDAPIO, NOME, IMAGEM, ORDEM) VALUES(8, 1, 'Sobremesas', 'ice-cream', 8);
INSERT INTO easycardapio.cardapio_secao (id_cardapio_secao, id_CARDAPIO, NOME, IMAGEM, ORDEM) VALUES(9, 1, 'Adicionais', 'pricetags', 9);
INSERT INTO easycardapio.cardapio_secao (id_cardapio_secao, id_CARDAPIO, NOME, IMAGEM, ORDEM) VALUES(10, 2, 'Pizzas', 'fast-food', 1);
INSERT INTO easycardapio.cardapio_secao (id_cardapio_secao, id_CARDAPIO, NOME, IMAGEM, ORDEM) VALUES(11, 2, 'Pizzas doces', 'ice-cream', 2);
INSERT INTO easycardapio.cardapio_secao (id_cardapio_secao, id_CARDAPIO, NOME, IMAGEM, ORDEM) VALUES(12, 2, 'Esfihas', 'fast-food', 3);
INSERT INTO easycardapio.cardapio_secao (id_cardapio_secao, id_CARDAPIO, NOME, IMAGEM, ORDEM) VALUES(13, 2, 'Esfihas doces', 'ice-cream', 4);

INSERT INTO easycardapio.conta (ID_CONTA, DATA_ABERTURA, DATA_FECHAMENTO) VALUES(1, '2021-02-22 10:15:01', NULL);
INSERT INTO easycardapio.conta (ID_CONTA, DATA_ABERTURA, DATA_FECHAMENTO) VALUES(2, '2021-02-22 10:12:21', '2021-02-22 11:00:55');

INSERT INTO easycardapio.cardapio_item (ID_CARDAPIO_ITEM, ID_CARDAPIO_SECAO, CODIGO_RESTAURANTE, NOME, IMAGEM, DESCRICAO, PRECO) VALUES(1, 1, '001', 'Coca-cola', 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f6/15-09-26-RalfR-WLC-0098.jpg/170px-15-09-26-RalfR-WLC-0098.jpg', NULL, 5.00);
INSERT INTO easycardapio.cardapio_item (ID_CARDAPIO_ITEM, ID_CARDAPIO_SECAO, CODIGO_RESTAURANTE, NOME, IMAGEM, DESCRICAO, PRECO) VALUES(2, 1, '002', 'Guaraná', 'https://logodownload.org/wp-content/uploads/2014/09/guarana-logo.jpg', NULL, 5.50);
INSERT INTO easycardapio.cardapio_item (ID_CARDAPIO_ITEM, ID_CARDAPIO_SECAO, CODIGO_RESTAURANTE, NOME, IMAGEM, DESCRICAO, PRECO) VALUES(3, 1, '003', 'Soda', 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/7b/Soda_Limonada.JPG/150px-Soda_Limonada.JPG', NULL, 4.80);

INSERT INTO easycardapio.cardapio_item_opcional (ID_CARDAPIO_ITEM_OPCIONAL, NOME) VALUES(1, 'com gelo');
INSERT INTO easycardapio.cardapio_item_opcional (ID_CARDAPIO_ITEM_OPCIONAL, NOME) VALUES(2, 'com limão');
INSERT INTO easycardapio.cardapio_item_opcional (ID_CARDAPIO_ITEM_OPCIONAL, NOME) VALUES(3, 'com laranja');

INSERT INTO easycardapio.cardapio_ci_cio (ID_CARDAPIO_CI_CIO, CARDAPIO_ITEM, CARDAPIO_ITEM_OPCIONAL) VALUES(1, 1, 1);
INSERT INTO easycardapio.cardapio_ci_cio (ID_CARDAPIO_CI_CIO, CARDAPIO_ITEM, CARDAPIO_ITEM_OPCIONAL) VALUES(2, 1, 2);
INSERT INTO easycardapio.cardapio_ci_cio (ID_CARDAPIO_CI_CIO, CARDAPIO_ITEM, CARDAPIO_ITEM_OPCIONAL) VALUES(3, 2, 1);
INSERT INTO easycardapio.cardapio_ci_cio (ID_CARDAPIO_CI_CIO, CARDAPIO_ITEM, CARDAPIO_ITEM_OPCIONAL) VALUES(4, 2, 3);
INSERT INTO easycardapio.cardapio_ci_cio (ID_CARDAPIO_CI_CIO, CARDAPIO_ITEM, CARDAPIO_ITEM_OPCIONAL) VALUES(5, 3, 1);
INSERT INTO easycardapio.cardapio_ci_cio (ID_CARDAPIO_CI_CIO, CARDAPIO_ITEM, CARDAPIO_ITEM_OPCIONAL) VALUES(6, 3, 2);


