package br.com.easymind.repository.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
@Table(schema = "easycardapio", name = "CONTA_PEDIDO_ITEM")
public class ContaPedidoItem {

	@Id
	@Column(name = "ID_CONTA_PEDIDO_ITEM")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idContaPedidoItem;

	@Column(name = "ID_CONTA")
	private Integer idConta;
	
	@Column(name = "ID_CONTA_PEDIDO")
	private Integer idContaPedido;

	@ManyToOne
	@JoinColumn(name = "ID_CARDAPIO_ITEM")
	private CardapioItem cardapioItem;
	
	@Column(name = "ID_CARDAPIO_ITEM_OPCIONAL")
	private Integer idCardapioItemOpcional;
	
	@Column(name = "CONTA_ADICIONAL")
	private String contaAdicional;
	
	@Column(name = "IS_READY")
	private Boolean isReady;
	

}