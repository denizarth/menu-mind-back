package br.com.easymind.repository;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import br.com.easymind.repository.data.CardapioSecao;

public interface CardapioSecaoRepository extends JpaRepository<CardapioSecao, Integer> {

	List<CardapioSecao> findAllByIdCardapio(Integer idCardapio, Sort by);
}