package br.com.easymind.repository.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
@Table(schema = "easycardapio", name = "CARDAPIO_SECAO")
public class CardapioSecao {

	@Id
	@Column(name = "ID_CARDAPIO_SECAO")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idCardapioSecao;

	@Column(name= "ID_CARDAPIO")
	private Integer idCardapio;
	
	@Column(name = "NOME")
	private String title;
	
	@Column(name = "IMAGEM")
	private String icon;
		
	@Column(name= "ORDEM")
	private Integer ordem;
}