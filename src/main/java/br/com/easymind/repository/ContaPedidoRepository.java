package br.com.easymind.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.easymind.http.data.ContaPedidoItem;
import br.com.easymind.repository.data.ContaPedido;

public interface ContaPedidoRepository extends JpaRepository<ContaPedido, Integer> {

	List<ContaPedido> findAllByIdConta(Integer id);
	/*
	@Query("SELECT CP.idContaPedido, CI.nome, \r\n" + 
			"  (SELECT CIO.nome FROM CardapioItemOpcional CIO WHERE CPI.idCardapioItemOpcional = CIO.idCardapioItemOpcional) \r\n" + 
			"FROM ContaPedido CP \r\n" + 
			"  JOIN ContaPedidoItem CPI ON CPI.idContaPedido = CP.idContaPedido\r\n" + 
			"  JOIN CardapioItem CI ON CI.idCardapioItem = CPI.idCardapioItem \r\n" + 
			"WHERE CP.idConta = 1")
			*/
	
	@Query(value = "SELECT \r\n" + 
			"  CP.ID_CONTA_PEDIDO, \r\n" + 
			"  CI.NOME ITEM, \r\n" + 
			"  (SELECT CIO.NOME FROM CARDAPIO_ITEM_OPCIONAL CIO WHERE CPI.ID_CARDAPIO_ITEM_OPCIONAL = CIO.ID_CARDAPIO_ITEM_OPCIONAL) OPCIONAL \r\n" + 
			"FROM CONTA_PEDIDO CP \r\n" + 
			"  JOIN CONTA_PEDIDO_ITEM CPI ON CPI.ID_CONTA_PEDIDO = CP.ID_CONTA_PEDIDO\r\n" + 
			"  JOIN CARDAPIO_ITEM CI ON CI.ID_CARDAPIO_ITEM = CPI.ID_CARDAPIO_ITEM \r\n" + 
			"WHERE CP.ID_CONTA = ?1", nativeQuery = true)	
	List<Object[]> findItensByIdConta(Integer idConta);

}
