package br.com.easymind.repository.data;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
@Table(schema = "easycardapio", name = "CONTA")
public class Conta {

	@Id
	@Column(name = "ID_CONTA")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idConta;

	@Column(name= "DATA_ABERTURA")
	private Date dataAbertura;
	
	@Column(name= "DATA_FECHAMENTO")
	private Date dataFechamento;

}