package br.com.easymind.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.easymind.repository.data.CardapioItem;

public interface CardapioItemRepository extends JpaRepository<CardapioItem, Integer> {

	List<CardapioItem> findAllByIdCardapioSecao(Integer idCardapioSecao);

	List<CardapioItem> findAllByIdCardapio(Integer idCardapio);

}
