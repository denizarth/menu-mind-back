package br.com.easymind.repository.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
@Table(schema = "easycardapio", name = "CARDAPIO_ITEM_OPCIONAL")
public class CardapioItemOpcional {

	@Id
	@Column(name = "ID_CARDAPIO_ITEM_OPCIONAL")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idCardapioItemOpcional;

	@Column(name = "IS_OBRIGATORIO")
	private Boolean isObrigatorio;

	@Column(name = "NOME")
	private String nome;

}