package br.com.easymind;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MenuMindBackApplication {

	public static void main(String[] args) {
		SpringApplication.run(MenuMindBackApplication.class, args);
	}

}
