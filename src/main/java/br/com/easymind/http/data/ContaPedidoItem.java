package br.com.easymind.http.data;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class ContaPedidoItem {

	private Integer idContaPedido;
	private String item;
	private String opcional;

}