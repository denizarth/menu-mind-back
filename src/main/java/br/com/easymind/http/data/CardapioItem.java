package br.com.easymind.http.data;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class CardapioItem {

	private Integer idCardapioItem;
	private Integer idCardapio;
	private Integer idCardapioSecao;
	private String nome;
	private String codigoRestaurante;
	private String imagem;
	private String[] itemAdicional; 
	private Double preco;
		

}