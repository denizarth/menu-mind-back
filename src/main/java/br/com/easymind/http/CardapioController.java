package br.com.easymind.http;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.easymind.repository.CardapioItemRepository;
import br.com.easymind.repository.CardapioSecaoRepository;
import br.com.easymind.repository.data.CardapioItem;
import br.com.easymind.repository.data.CardapioSecao;
import lombok.RequiredArgsConstructor;

@CrossOrigin(origins="*", allowedHeaders="*")
@RequestMapping(path = "cardapio")
@RestController
@RequiredArgsConstructor
public class CardapioController {

	private final CardapioSecaoRepository catalogoSecaoRepository;
	private final CardapioItemRepository cardapioItemRepository;
    
	@GetMapping("abas")
	public @ResponseBody List<CardapioSecao> getAbas(@RequestParam Integer idCardapio) {
		//catalogoSecaoRepository.findAll().forEach(item -> {System.out.println(item);});
		return catalogoSecaoRepository.findAllByIdCardapio(idCardapio, Sort.by(Sort.Direction.ASC, "ordem"));
	}
	
	@GetMapping("itens")
	public @ResponseBody List<CardapioItem> getItem(@RequestParam Integer idCardapio) {
		return cardapioItemRepository.findAll();
	}
}
