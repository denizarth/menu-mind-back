package br.com.easymind.repository.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
@Table(schema = "easycardapio", name = "CARDAPIO_CI_CIO")
public class CardapioCiCio {

	@Id
	@Column(name = "ID_CARDAPIO_CI_CIO")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idCardapioCiCio;
/*
	@ManyToOne
	@JoinColumn(name = "ID_CARDAPIO_ITEM")
	private CardapioItem cardapioItem;
	*/
	
	@ManyToOne
	@JoinColumn(name = "ID_CARDAPIO_ITEM_OPCIONAL")
	private CardapioItemOpcional cardapioItemOpcional;
	
	/*
	@Column(name= "ID_CARDAPIO_ITEM_OPCIONAL")
	private Integer idCardapioItemOpcional;
	*/
}